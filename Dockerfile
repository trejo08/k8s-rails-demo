FROM ruby:2.7.1-alpine

## TODO: [DO-21] Make a new Dockerfile for production and development
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh build-base postgresql-dev tzdata nodejs npm less

# Configure the main working directory. This is the base
# directory used in any further RUN, COPY, and ENTRYPOINT
# commands.
RUN mkdir -p /app
WORKDIR /app

## To avoid warnings at rails initialization time or run commands
ENV RUBYOPT="-W:no-deprecated -W:no-experimental"
ENV RAILS_ENV=development

# Copy the Gemfile as well as the Gemfile.lock and install
# the RubyGems. This is a separate step so the dependencies
# will be cached unless changes to one of those two files
# are made.
ADD Gemfile* ./
RUN gem install bundler
RUN bundle install --jobs 8 --retry 5

# Copy the main application.
ADD . .

ENTRYPOINT ["bin/entrypoint"]
EXPOSE 3000

# Start puma
CMD ["bin/rails", "server", "-b", "0.0.0.0"]
